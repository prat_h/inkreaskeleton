var path = require('path');
var lrSnippet = require('grunt-contrib-livereload/lib/utils').livereloadSnippet;

var folderMount = function folderMount(connect, point) {
	return connect.static(path.resolve(point));
};

module.exports = function( grunt ) {
	'use strict';

    /**
	 * load NPM tasks
	 */

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-regarde');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-livereload');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-open');
    grunt.loadNpmTasks('grunt-replace');
    grunt.loadNpmTasks('grunt-usemin');
    grunt.loadNpmTasks('grunt-env');

    /**
     * tasks and targets
     */
    grunt.initConfig({

		//clean all the builded files
		clean : ["dist", "css/main.css"],

		copy: {
			build: {
				files: [
					{src: ['*.html'], dest: 'temp/'}
				]
			},
			dev: {
				options: {
					processContentExclude:[ 'temp/' ]
				},
				files: [
					{src: ['temp/*.html'], dest: './'}
				]
			}
		},

		//open the file
		open : {
			dev : {
				path : 'http://localhost:9001'
			}
		},

		//connect to the server for livereloading
		connect: {
			livereload: {
				options: {
					port: 9001,
					middleware: function(connect, options) {
						return [lrSnippet, folderMount(connect, '.')];
					}
				}
			}
		},

		//less tasks and target
		less : {
			development: {
				options: {
					paths: ["css/less/includes"]
				},
				files: {
					"css/main.css": "css/less/style.less"
				}
			},
			production: {
				options: {
					paths: ["css/less/includes"],
					compress: true
				},
				files: {
					"dist/css/main-min.css": "css/less/style.less"
				}
			}
		},
		// Configuration to be run (and then tested) almost the same as watch
		regarde: {
			less : {
				files : [
					'css/**/*.less'
				],
				tasks : ['livereload', 'less']
			},
			js : {
				files : [
					'js/**/*.js', 'Gruntfile.js'
				],
				tasks : ['livereload']
			}
		},

		useminPrepare: {
			html: '*.html'
		},

		usemin: {
			html: ['*.html'],
			css: ['dist/css/**/*.css']
		}

    });

	//

    /**
     * register tasks to perform upon build
     */
    grunt.registerTask('default', ['open', 'clean', 'less', 'copy:dev', 'livereload-start', 'connect', 'regarde']);
    grunt.registerTask('build', ['open', 'clean',  'less', 'copy:build', 'useminPrepare','usemin', 'livereload-start', 'connect', 'regarde']);
    grunt.registerTask('phpfile', ['clean', 'less', 'regarde']);

};
